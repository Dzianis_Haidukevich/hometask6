package Task3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int a, b, c, SCM;

        a = inputIntFromConsole("A: ");
        b = inputIntFromConsole("B: ");
        c = inputIntFromConsole("C: ");

        SCM = findThreeNumbersSCM(a,b,c); // NOK

        resultPrint(SCM);
    }
    public static int inputIntFromConsole(String message) {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Invalid input " + message);
        }
        value = sc.nextInt();
        return value;
    }
    public static int findGCD(int a, int b) {
        int GCD;
        while (a != b) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        GCD = a;
        return GCD;
    }

    public static int findSCM(int a, int b) {
        int SCM;
        int GCD = findGCD(a, b);
        SCM = ((a * b) / GCD);
        return SCM;
    }

    public static int findThreeNumbersSCM(int a, int b, int c){
        int threeNumSCM, primarySCM;
        primarySCM = findSCM(a,b);
        threeNumSCM = findSCM(primarySCM,c);  // recursive method
        return threeNumSCM;
    }
    public static void resultPrint(int SCM) {
        System.out.printf(SCM + " " );
    }
}