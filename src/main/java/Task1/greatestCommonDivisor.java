package Task1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.Scanner;

public class greatestCommonDivisor {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> denominators = new ArrayList<>();
        ArrayList<Integer> denominators1 = new ArrayList<>();

        int i = sc.nextInt();
        int b = sc.nextInt();

        for (int j = 1; j < i; j++) {
            double a = (double) i / (double) j;
            if ((a - Math.floor(a)) == 0.0)
                denominators.add(j);
        }
        for (int j = 1; j < b; j++) {
            double a = (double) b / (double) j;
            if ((a - Math.floor(a)) == 0.0)
                denominators1.add(j);
        }
        Collections.reverse(denominators);
        Collections.reverse(denominators1);

        boolean result = false;
        for (Integer denominator : denominators) {
            if (result)
                break;
            for (Integer integer : denominators1) {
                if (Objects.equals(denominator, integer)) {
                    result = true;
                    System.out.println(denominator);
                    break;
                }
            }
        }
    }
}
